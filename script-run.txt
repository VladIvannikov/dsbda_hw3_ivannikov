# Sum of twitter's message by country in minute.

## How to start

* Start ElasticSearch-Logstash-Grafana

  docker-compose up -d ElasticSearch
  
  docker-compose up -d logstash
  
  docker-compose up -d grafana
  
* Run it
	
	docker-compose up -d my-java-app

# Run it in vagrant

You may see result on `127.0.0.1:3000`