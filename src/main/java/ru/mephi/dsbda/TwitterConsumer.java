package ru.mephi.dsbda;

import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.logging.Logger;

public class TwitterConsumer {

    private static java.util.logging.Logger logger = Logger.getLogger(TwitterConsumer.class.getName());

    private Socket socket;
    private Format formatter;
    private RawStreamListener listener = null;

    // Empty constructor TwitterConsumer
    public TwitterConsumer() {
        formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    }
    // get Formatter
    public Format getFormatter() {

        return formatter;
    }
    // close socket channel
    private void close() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // create Json message from twitter Status
    public String getJsonFromStatus(Status status) {
        String createdAt = formatter.format(status.getCreatedAt());
        JSONObject jsonString = new JSONObject()
                .put("country", status.getPlace().getCountry())
                .put("created_at", createdAt);
        return jsonString.toString();
    }


    // start Listen twitter
    public void startListen() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(Settings.TWITTER_CONSUMER_KEY)
                .setOAuthConsumerSecret(Settings.TWITTER_CONSUMER_SECRET)
                .setOAuthAccessToken(Settings.TWITTER_ACCESS_TOKEN)
                .setOAuthAccessTokenSecret(Settings.TWITTER_ACCESS_TOKEN_SECRET);

        TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
        FilterQuery filtor = new FilterQuery();
        // all Earth
        double[][] bb = {{-180, -90}, {180, 90}};
        filtor.locations(bb);
        twitterStream.addListener(listener);
        twitterStream.filter(filtor);
    }

    // inicialiaze listener
    public void setRawListener() {
        listener = new RawStreamListener() {
            @Override
            public void onMessage(String s) {
                try {
                    socket = new Socket(Settings.LOGSTASH_URL, Settings.LOGSTASH_PORT);
                } catch (IOException e) {

                    return;
                }

                try {
                    DataOutputStream os = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
                    os.writeBytes(getJsonFromStatus(TwitterObjectFactory.createStatus(s)));
                    os.flush();
                } catch (IOException | TwitterException e) {
                    e.printStackTrace();
                }
                close();

            }

            @Override
            public void onException(Exception e) {
                e.printStackTrace();
            }
        };

    }

}
