package ru.mephi.dsbda;

// main settings of application, twitter tokens

public class Settings {
    public static String LOGSTASH_URL = "172.18.0.1";
    public static Integer LOGSTASH_PORT = 5044;

    public static final String TWITTER_CONSUMER_KEY = "test";
    public static final String TWITTER_CONSUMER_SECRET = "test";
    public static final String TWITTER_ACCESS_TOKEN = "test";
    public static final String TWITTER_ACCESS_TOKEN_SECRET = "test";


}
