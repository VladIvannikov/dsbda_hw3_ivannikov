package ru.mephi.dsbda;


import java.util.logging.Logger;

public class Main {

    private static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
            TwitterConsumer twitterConsumer = new TwitterConsumer();
            twitterConsumer.setRawListener();
            twitterConsumer.startListen();

    }
}
