FROM maven:3.5.2-jdk-8-alpine

WORKDIR /app
COPY . .
RUN ls
RUN mvn install
RUN ls /app/target
RUN chmod +x ./run.sh
RUN bash ./run.sh